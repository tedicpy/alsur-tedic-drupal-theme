(function ($, Drupal) {
  Drupal.behaviors.mtWaypointsAnimations = {
    attach: function (context, settings) {
      var element = once('mtWaypointsAnimations', 'body:not(.path-admin) [data-animate-effect]', context);
      element.forEach(function() {
        var thisObject = $(this);
        var animation = thisObject.attr("data-animate-effect");
        if(animation != "no-animation") {
          var waypoints = thisObject.waypoint(function(direction) {
            var animatedObject = $(this.element);
            setTimeout(function() {
              animatedObject.addClass("animated " + animation);
            }, 100);
            this.destroy();
          },{
            offset: "90%"
          });
        }
      });
    }
  };
})(jQuery, Drupal);
