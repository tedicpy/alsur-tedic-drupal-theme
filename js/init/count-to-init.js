(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.mtCountTo = {
    attach: function (context, settings) {
      var element = once('mtCountToInit', '[data-to]', context);
      element.forEach(function() {
        var stat_item = $(this);
        var waypoints = stat_item.waypoint(function(direction) {
          var animatedObject = $(this.element);
            animatedObject.countTo();
            this.destroy();
          },{
            offset: "90%"
          });
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
