# Tema Drupal para AlSur

Tema para Drupal 8 creado por Tedic para la web de AlSur

Es heredado de Magazine Lite con bastantes modificaciones: https://github.com/morethanthemes/magazine-lite

## Archivos importantes

En la raíz del tema:

**logo** logo.svg - este logo es para la versión móvil "responsive"

**alsur.theme** - Aquí podemos utilizar la plantilla para cambiar los fondos de cada bloque... por ejemplo:

En la linea 594 - `// Region Background Color` podemos ver el bloque `slideout_background_color` que está en fucsia-fondo que ya está configurado en el css, más adelante explico un poco el CSS.

## Hoja de estilos

El tema venía con css por cada feature pero los retoques se centralizaron en el css principal de modificaciones: css/themes/theme/local-theme.css

Ahí están definidos los colores del sistema:
- naranja
- fucsia
- blanco
- azul
- azulm
- rosa
- amarillo

Y luego está más o menos documentado por bloques:
- Menú.
- Botones
- Content top
- Idiomas
- Drupal modal
- Otros bloques

Otra parte importante es que el drupal funciona con las views, multilenguaje, y con 4 tipos de contenido:
- Artículos, miembros, páginas básicas y reportes...

Por lo que hay que ajustarse a las características para poder replicar la página.